package simulations;
import processing.core.PApplet;
import behaviors.AlignToVelocity;
import behaviors.BadWander;
import behaviors.BreadCrumbs;
import behaviors.CapAcceleration;
import behaviors.CapRotation;
import behaviors.CapVelocity;
import behaviors.StayOnScreen;
import entities.Boid;


public class BadWanderSim extends Simulation {
	
	public String getName() {return "Wander";};
	
	public BadWanderSim() {
		super();
		engine.addEntity(new Boid(200, 200,
				new BreadCrumbs(40, 60),
				
				new BadWander(200, 60, 100, 120, 1),
				new StayOnScreen(80, 1),
				new AlignToVelocity(),
				
				new CapAcceleration(.2f),
				new CapRotation(PApplet.radians(4)),
				new CapVelocity(3f)
			));
		engine.begin();
	}
	
	
	public static void main(String[] args) {
		new BadWanderSim();
	}
}

package simulations;

import java.util.Random;

import core.Engine;
import processing.core.PApplet;
import behaviors.AlignToVelocity;
import behaviors.AvoidEntities;
import behaviors.BreadCrumbs;
import behaviors.CapAcceleration;
import behaviors.CapRotation;
import behaviors.CapVelocity;
import behaviors.SeekCenterOfMass;
import behaviors.SeekClosestEntity;
import behaviors.SeekEntity;
import behaviors.StayOnScreen;
import behaviors.Wander;
import entities.Boid;
import entities.Entity;

public class FlockSimDouble extends Simulation {

	public String getName() {return "Flock";};
	
	public FlockSimDouble() {
		super();
		AvoidEntities avoid = new AvoidEntities(10, 40, 4f);
		SeekCenterOfMass center = new SeekCenterOfMass(150, 3f);
		SeekClosestEntity closest = new SeekClosestEntity(4f);
		
		Boid kingBoid = new Boid(500, 400, 20, 0xffaa00aa, 7,
				new BreadCrumbs(40, 60),
				
				new Wander(150, 100, 40, 1, 1),
				new StayOnScreen(100, 3f),
				new AlignToVelocity(),
				
				new CapAcceleration(.2f),
				new CapRotation(PApplet.radians(4)),
				new CapVelocity(4f)
		);
		
		Boid kingBoid2 = new Boid(200, 400, 20, 0xffff6600, 7,
				new BreadCrumbs(40, 60),
				
				new Wander(150, 100, 40, 1, 1),
				new StayOnScreen(100, 3f),
				new AlignToVelocity(),
				
				new CapAcceleration(.2f),
				new CapRotation(PApplet.radians(4)),
				new CapVelocity(4f)
		);

		Random r = new Random();
		for (int i = 0; i < 80; i++) {
			engine.addEntity(new Boid(r.nextInt(Engine.x()), r.nextInt(Engine.y()), 1, 0xff0000ff, 5,
					avoid,
					center,
					closest,
					new StayOnScreen(80, 1),
					new AlignToVelocity(),
					
					//new BreadCrumbs(100, 5),
					
					new CapAcceleration(.1f),
					new CapRotation(PApplet.radians(4)),
					new CapVelocity(4f)
	
				));
		}
		
		for (Entity e : engine.entities()) {
			avoid.addEntity(e);
			center.addEntity(e);
		}
		
		avoid.addEntity(kingBoid);
		center.addEntity(kingBoid);
		closest.addEntity(kingBoid);
		
		avoid.addEntity(kingBoid2);
		center.addEntity(kingBoid2);
		closest.addEntity(kingBoid2);
		
		engine.addEntity(kingBoid);
		engine.addEntity(kingBoid2);
		engine.begin();
	}
	
	public static void main(String[] args) {
		new FlockSimDouble();
	}
	
}

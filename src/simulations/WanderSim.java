package simulations;
import processing.core.PApplet;
import behaviors.AlignToVelocity;
import behaviors.BreadCrumbs;
import behaviors.CapAcceleration;
import behaviors.CapRotation;
import behaviors.CapVelocity;
import behaviors.StayOnScreen;
import behaviors.Wander;
import entities.Boid;


public class WanderSim extends Simulation {
	
	public String getName() {return "Wander";};
	
	public WanderSim() {
		super();
		engine.addEntity(new Boid(200, 200,
				new BreadCrumbs(40, 60),
				
				new Wander(200, 60, 100, 4, 1),
				new StayOnScreen(80, 1),
				new AlignToVelocity(),
				
				new CapAcceleration(.1f),
				new CapRotation(PApplet.radians(2)),
				new CapVelocity(1.5f)
			));
		engine.begin();
	}
	
	
	public static void main(String[] args) {
		new WanderSim();
	}
}

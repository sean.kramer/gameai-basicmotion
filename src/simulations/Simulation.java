package simulations;
import core.Engine;
import processing.event.KeyEvent;
import processing.event.MouseEvent;


public abstract class Simulation {

	protected Engine engine;
	
	public Simulation() {
		engine = Engine.boot(this);
	}

	public abstract String getName();
	
	public void keyPressed(KeyEvent e) {}
	
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == java.awt.event.KeyEvent.VK_TAB)
			Engine.drawBehaviors = !Engine.drawBehaviors;
	}
	
	public void mousePressed(MouseEvent e) {};
	public void mouseReleased(MouseEvent e) {};
	public void mouseDragged(MouseEvent e) {};
	public void mouseMoved(MouseEvent e) {};

}

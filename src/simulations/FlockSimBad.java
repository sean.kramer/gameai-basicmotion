package simulations;

import java.util.Random;

import core.Engine;
import processing.core.PApplet;
import behaviors.AlignToVelocity;
import behaviors.AvoidEntities;
import behaviors.BreadCrumbs;
import behaviors.CapAcceleration;
import behaviors.CapRotation;
import behaviors.CapVelocity;
import behaviors.SeekCenterOfMass;
import behaviors.SeekEntity;
import behaviors.StayOnScreen;
import behaviors.Wander;
import entities.Boid;
import entities.Entity;

public class FlockSimBad extends Simulation {

	public String getName() {return "Flock";};
	
	public FlockSimBad() {
		super();
		AvoidEntities avoid = new AvoidEntities(10, 20, 3f);
		SeekCenterOfMass center = new SeekCenterOfMass(200, .5f);
		
		Boid kingBoid = new Boid(500, 400, 5, 0xffaa00aa, 7,
				new BreadCrumbs(40, 60),
				
				new Wander(200, 60, 100, 4, 1),
				new StayOnScreen(80, 1),
				new AlignToVelocity(),
				
				new CapAcceleration(.15f),
				new CapRotation(PApplet.radians(2)),
				new CapVelocity(1.5f)
				
		);

		Random r = new Random();
		for (int i = 0; i < 60; i++) {
			engine.addEntity(new Boid(r.nextInt(Engine.x()), r.nextInt(Engine.y()), 1, 0xff0000ff, 5,
					avoid,
					center,
					new SeekEntity(kingBoid, .5f),
					new StayOnScreen(80, 1),
					new AlignToVelocity(),
					
					new BreadCrumbs(100, 5),
					
					new CapAcceleration(.15f),
					new CapRotation(PApplet.radians(2)),
					new CapVelocity(1.5f)
	
				));
		}
		
		for (Entity e : engine.entities()) {
			avoid.addEntity(e);
			center.addEntity(e);
		}
		
		avoid.addEntity(kingBoid);
		center.addEntity(kingBoid);
		
		engine.addEntity(kingBoid);
		engine.begin();
	}
	
	public static void main(String[] args) {
		new FlockSimBad();
	}
	
}

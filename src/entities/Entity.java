package entities;
import java.util.ArrayList;

import behaviors.Behavior;
import processing.core.PApplet;
import processing.core.PVector;


public abstract class Entity {
	
	protected PVector position;
	protected PVector velocity;
	protected float rotation;
	protected float mass;
	
	protected PVector deltaAcceleration;
	protected float deltaRotation;

	protected ArrayList<Behavior> behaviors;
	
	public Entity(float x, float y, Behavior... behaviors) {
		this(x, y, 1, 0, behaviors);
	}
	
	
	public Entity(float x, float y, float m, Behavior... behaviors) {
		this(x, y, m, 0, behaviors);
	}
	
	
	public float getMass() {
		return mass;
	}
	
	public Entity(float x, float y, float m, float r, Behavior... behaviors) {
		position = new PVector(x, y);
		velocity = new PVector(0, 0);
		rotation = r;
		mass = m;
		
		deltaAcceleration = new PVector(0, 0);
		deltaRotation = 0;
		
		this.behaviors = new ArrayList<Behavior>(behaviors.length);
		for (Behavior b : behaviors)
			this.behaviors.add(b);
	}
	
	
	
	public abstract void drawCrumb(PApplet g, float x, float y, float r);
	
	
	public void update() {
		rotation += deltaRotation;
		deltaRotation = 0;
		
		if (rotation > PApplet.PI)
			rotation -= PApplet.TWO_PI;
		else if (rotation <= -PApplet.PI)
			rotation += PApplet.TWO_PI;
		
		velocity.add(deltaAcceleration);
		deltaAcceleration.mult(0);
		
		position.add(velocity);
	}
	
	
	public void applyBehaviors(PApplet g) {
		for (Behavior b : behaviors)
			b.apply(this, g);
	}
	
	
	/** For getting current values. */
	public float rotation() {return rotation;}
	public PVector velocity() {return velocity;}
	public PVector position() {return position;}
	
	/** For progressively adjusting deltas */
	public void rotate(float rotation) {deltaRotation += rotation;}
	public void accelerate(PVector acceleration) {deltaAcceleration.add(acceleration);}
	
	/** For getting delta values. */
	public float rotationDelta() {return deltaRotation;}
	public PVector acceleration() {return deltaAcceleration;}
	
	
	public void addBehavior(Behavior behavior) {
		behaviors.add(behavior);
	}
	
	
	public abstract void draw(PApplet g);
}

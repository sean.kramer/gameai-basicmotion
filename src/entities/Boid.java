package entities;
import behaviors.Behavior;
import processing.core.PApplet;


public class Boid extends Entity {
	public static final float CRUMB_SCALE = 0.4f;
	public static final int DEFAULT_SIZE = 10;
	
	//Color
	private int c;
	
	private float radius = 10;
	private float diameter = radius * 2;
	private float tx = radius * PApplet.cos(PApplet.radians(60));
	private float ty = radius * PApplet.sin(PApplet.radians(60));
	
	private float bdiameter = diameter * CRUMB_SCALE;
	private float btx = tx * CRUMB_SCALE;
	private float bty = ty * CRUMB_SCALE;
	
	private void assign() {
		diameter = radius * 2;
		tx = radius * PApplet.cos(PApplet.radians(60));
		ty = radius * PApplet.sin(PApplet.radians(60));

		bdiameter = diameter * CRUMB_SCALE;
		btx = tx * CRUMB_SCALE;
		bty = ty * CRUMB_SCALE;
	}
	
	public Boid(float x, float y, float m, int c, Behavior... behavior) {
		this(x, y, m, c, DEFAULT_SIZE, behavior);
	}
	
	
	public Boid(float x, float y, float m, int c, int r, Behavior... behavior) {
		super(x, y, m, behavior);
		this.c = c;
		radius = r;
		assign();
	}
	
	
	public Boid(float x, float y, Behavior... behaviors) {
		this(x, y, 1, 0xff000000, behaviors);
	}
	
	
	@Override
	public void draw(PApplet g) {
		g.noStroke();
		g.fill(c);
		g.pushMatrix();
		g.translate(position.x, position.y);
		g.ellipse(0, 0, diameter, diameter);
		g.rotate(rotation);
		g.triangle(tx, -ty, diameter, 0, tx, ty);
		g.popMatrix();
	}


	@Override
	public void drawCrumb(PApplet g, float x, float y, float r) {
		g.noStroke();
		g.fill(c);
		g.pushMatrix();
		g.translate(x, y);
		g.ellipse(0, 0, bdiameter, bdiameter);
		g.rotate(r);
		g.triangle(btx, -bty, bdiameter, 0, btx, bty);
		g.popMatrix();
	}
}

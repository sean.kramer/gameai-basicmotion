import processing.core.PApplet;


public class Hinges extends PApplet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void setup() {
		size(300, 300);
		frame.setResizable(true);
		frameRate(60);
		ellipseMode(CENTER);
		noFill();
		smooth();
	}
	
	float i = -HALF_PI;
	
	public void draw() {
		if (i > HALF_PI) return;
		g.translate(100, 100);
		g.rotate(i);
		g.line(0, 0, i + 2, i);
		g.translate(25, 0);
		g.rotate(0);
		g.line(0, 0, 0, 2);
		g.arc(0, 0, 50, 50, radians(-160), radians(-20));
		i += radians(1);
	}
	
	public static void main(String[] args) {
		PApplet.main("Hinges");
	}
	
}

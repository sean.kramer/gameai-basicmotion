package behaviors;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;

public class CapAcceleration extends Behavior {

	public final int DRAW_FACTOR = 40;
	
	private float cap;
	private float capSq;
	private float draw;
	
	public CapAcceleration() {
		cap(0);
	}	
	
	public CapAcceleration(float cap) {
		cap(cap);
	}
	
	
	public void cap(float cap) {
		this.cap = cap;
		this.capSq = cap * cap;
		this.draw = 1 / cap * DRAW_FACTOR;
	}
	
	
	public void apply(Entity boid, PApplet g) {
		PVector acceleration = boid.acceleration();
		
		if (acceleration.magSq() > capSq) {
			acceleration.normalize();
			acceleration.mult(cap);
		}
		
		
		if (draw()) {
			g.strokeWeight(2);
			g.stroke(0xff9933ff);
			g.line(boid.position().x, boid.position().y,
					boid.position().x + acceleration.x * draw, boid.position().y + acceleration.y * draw);
		}
	}
}

package behaviors;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;

public class AvoidEntities extends BlendableBehavior implements MultiEntityBehavior {

	private float radius;
	private float buffer;
	ArrayList<Entity> avoid;
	
	
	public AvoidEntities(float buffer, float radius, float strength) {
		super(strength);
		avoid = new ArrayList<Entity>();
		this.radius = radius;
		this.buffer = radius - buffer;
	}
	
	
	@Override
	public void apply(Entity boid, PApplet g) {

		g.stroke(0xff800000);
		for (Entity e : avoid) {
			if (e == boid) continue;
			float dist = PVector.dist(boid.position(), e.position());
			if (dist > radius) continue;
			
			PVector acceleration = PVector.sub(boid.position(), e.position());
			acceleration.normalize();
			
			float calc = (radius - dist) / buffer;
			acceleration.mult(calc * strength);
			boid.accelerate(acceleration);

			if (draw()) {
				g.stroke(200, 0, 0, calc * 255);
				g.strokeWeight(2);
				g.line(boid.position().x, boid.position().y, e.position().x, e.position().y);
			}
		}
	}


	@Override
	public void addEntity(Entity entity) {
		avoid.add(entity);
	}

}

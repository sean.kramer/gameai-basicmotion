package behaviors;

import processing.core.PApplet;
import entities.Entity;

public class SeekEntity extends BlendableBehavior {
	
	private Entity entity;
	private SeekTarget seeker;
	
	
	public SeekEntity(Entity entity, float strength) {
		super(strength);
		this.entity = entity;
		this.seeker = new SeekTarget(entity.position().x, entity.position().y, strength);
		addSubBehavior(seeker);
	}
	
	
	@Override
	public void apply(Entity boid, PApplet g) {
		seeker.target(entity.position().x, entity.position().y);
		applySubBehaviors(boid, g);
	}
	
}

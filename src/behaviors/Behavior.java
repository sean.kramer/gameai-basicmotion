package behaviors;

import java.util.ArrayList;

import core.Engine;
import processing.core.PApplet;
import entities.Entity;


public abstract class Behavior {

	//Acceleration factor, for drawing
	public static final float AF = 100;
	
	protected ArrayList<Behavior> subBehaviors = null;
	

	protected void addSubBehavior(Behavior behavior) {
		if (subBehaviors == null)
			subBehaviors = new ArrayList<Behavior>();
		subBehaviors.add(behavior); 
	}
	
	protected void applySubBehaviors(Entity boid, PApplet g) {
		for (Behavior b : subBehaviors)
			b.apply(boid, g);
	}
	
	public abstract void apply(Entity boid, PApplet g);
	
	
	protected boolean draw() {return Engine.drawBehaviors;}
	
}

package behaviors;



public abstract class BlendableBehavior extends Behavior {

	protected float strength;
	
	public BlendableBehavior(float strength) {
		setStrength(strength);
	}
	
	
	public BlendableBehavior() {
		this(1);
	}
	
	
	public void setStrength(float strength) {
		this.strength = strength;
		if (subBehaviors != null) {
			for (Behavior b : subBehaviors) {
				if (b instanceof BlendableBehavior)
					((BlendableBehavior) b).setStrength(strength);
			}
		}
	}
	
	
	public float getStrength() {
		return strength;
	}
	
}

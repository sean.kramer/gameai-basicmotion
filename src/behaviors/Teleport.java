package behaviors;

import java.util.Random;

import processing.core.PApplet;
import entities.Entity;

public class Teleport extends Behavior {

	private int rarity;
	private Random r = new Random();
	
	public Teleport(int rarity) {
		this.rarity = rarity;
	}
	
	@Override
	public void apply(Entity boid, PApplet g) {
		if (r.nextInt(rarity) == 0) {
			boid.position().x = r.nextInt(g.width);
			boid.position().y = r.nextInt(g.height);		
			g.background(0xffffffff);
		}
	}

	
}

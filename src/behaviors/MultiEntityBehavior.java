package behaviors;

import entities.Entity;

public interface MultiEntityBehavior {

	public void addEntity(Entity entity);
	
}

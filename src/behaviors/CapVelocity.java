package behaviors;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;

public class CapVelocity extends Behavior {
	
	public final int DRAW_FACTOR = 80;
	
	private float cap;
	private float capSq;
	private float draw;
	
	public CapVelocity() {
		cap(0);
	}	
	
	public CapVelocity(float cap) {
		cap(cap);
	}
	
	
	public void cap(float cap) {
		this.cap = cap;
		this.capSq = cap * cap;
		this.draw = 1 / cap * DRAW_FACTOR;
	}
	
	
	public void apply(Entity boid, PApplet g) {
		PVector velocity = boid.velocity();
		velocity.add(boid.acceleration());
		if (velocity.magSq() > capSq) {
			velocity.normalize();
			velocity.mult(cap);
			boid.acceleration().mult(0);
		}
		
		if (draw()) {
			g.strokeWeight(1);
			g.stroke(0xff3366ff);
			g.line(boid.position().x, boid.position().y,
					boid.position().x + velocity.x * draw, boid.position().y + velocity.y * draw);
		}
	}
}

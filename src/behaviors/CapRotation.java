package behaviors;

import processing.core.PApplet;
import entities.Entity;

public class CapRotation extends Behavior {

	private float radians;
	
	public CapRotation() {cap(0);}	
	public CapRotation(float radians) {cap(radians);}
	
	
	public void cap(float radians) {
		this.radians = radians;
	}
	
	
	public void capDegrees(float degrees) {
		this.radians = PApplet.radians(degrees);
	}
	
	
	public void apply(Entity boid, PApplet g) {
		float rotation = boid.rotationDelta();
		if (Math.abs(rotation) > radians) {
			rotation = Math.signum(rotation) * radians - rotation;
			boid.rotate(rotation);
		}
	}
}

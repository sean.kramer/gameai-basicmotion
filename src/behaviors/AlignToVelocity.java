package behaviors;

import processing.core.PApplet;
import entities.Entity;

public class AlignToVelocity extends Behavior {
	
	@Override
	public void apply(Entity boid, PApplet g) {
		float desired = PApplet.atan2(boid.velocity().y, boid.velocity().x);
		float delta = desired - boid.rotation();
		
		if (delta > PApplet.PI)
			delta -= PApplet.TWO_PI;
		if (delta <= -PApplet.PI)
			delta += PApplet.TWO_PI;
		
		boid.rotate(delta);
	}
}

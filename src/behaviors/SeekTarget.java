package behaviors;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;

public class SeekTarget extends BlendableBehavior {

	private PVector target;
	
	public SeekTarget(float x, float y, float strength) {
		super(strength);
		target(x, y);
	}
	
	
	public void target(float x, float y) {
		target = new PVector(x, y);
	}
	
	
	@Override
	public void apply(Entity boid, PApplet g) {
		PVector desired = new PVector(target.x - boid.position().x, target.y - boid.position().y);
		desired.sub(boid.velocity());
		desired.normalize();
		desired.mult(strength);
		boid.accelerate(desired);
	}

	
}

package behaviors;

import java.util.Random;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;


public class Wander extends BlendableBehavior {

	private float seekDist = 200;
	private float seekRadius = 70;
	private float maxJumpRadians = PApplet.radians(80);
	private int smoothing = 3;
	
	public final Random RANDOM = new Random();

	private int buffer = 0;
	private float angle = 0;
	
	private SeekTarget seeker;
	
	/**
	 * Wander around aimlessly
	 * @param seekDist distance to center of seek-circle
	 * @param seekRadius radius of seek-circle
	 * @param maxJumpDegrees max degree change per update
	 * @param smoothing frames between updates
	 * @param strength power applied to the algorithm
	 */
	public Wander(float seekDist, float seekRadius, float maxJumpDegrees, int smoothing, float strength) {
		super(strength);
		this.seekDist = seekDist;
		this.seekRadius = seekRadius;
		this.smoothing = smoothing;
		maxJumpRadians = PApplet.radians(maxJumpDegrees);
		
		seeker = new SeekTarget(0, 0, strength);
		addSubBehavior(seeker);
	}
	
	
	@Override
	public void apply(Entity boid, PApplet g) {
		if (buffer == 0) {
			angle += (float) (Math.random() - 0.5) * maxJumpRadians;
			buffer = smoothing;
		} else {
			buffer--;
		}
		
		//Find target from circle projection
		PVector target = boid.velocity();
		if (target.magSq() == 0)
			target = PVector.fromAngle(boid.rotation());
		else
			target = target.get();
		
		target.normalize();
		target.mult(seekDist);
		
		PVector angular = PVector.fromAngle(angle + boid.rotation());
		angular.mult(seekRadius);
		
		target.add(boid.position());
		
		//Behavior drawing
		if (draw()) {
			g.noFill();
			g.strokeWeight(2);
			g.stroke(0x22ff0000);
			g.ellipse(target.x, target.y, seekRadius * 2, seekRadius * 2);
			g.line(boid.position().x, boid.position().y, target.x, target.y);
			g.noStroke();
			g.fill(0x22ff0000);
			g.ellipse(target.x + angular.x, target.y + angular.y, 20, 20);
		}

		target.add(angular);
		seeker.target(target.x, target.y);
	
		applySubBehaviors(boid, g);
	}
}

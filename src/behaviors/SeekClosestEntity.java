package behaviors;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;

public class SeekClosestEntity extends BlendableBehavior implements MultiEntityBehavior {

	private ArrayList<Entity> entities;
	private SeekTarget seeker;
	
	
	public SeekClosestEntity(float strength) {
		super(strength);
		this.seeker = new SeekTarget(0, 0, strength);
		entities = new ArrayList<Entity>();
		addSubBehavior(seeker);
	}

	
	@Override
	public void apply(Entity boid, PApplet g) {
		Entity closest;
		if (entities.size() == 0) {
			//Target self if no other targets
			closest = boid;
		} else {
			//Find closest target
			closest = entities.get(0);
			float dist = PVector.dist(boid.position(), closest.position());
			for (int i = 1; i < entities.size(); i++) {
				float tmp = PVector.dist(boid.position(), entities.get(i).position());
				if (tmp < dist) {
					closest = entities.get(i);
					dist = tmp;
				}
			}
		}
		seeker.target(closest.position().x, closest.position().y);
		applySubBehaviors(boid, g);
	}

	
	@Override
	public void addEntity(Entity entity) {
		entities.add(entity);
	}

}

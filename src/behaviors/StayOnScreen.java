package behaviors;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;

public class StayOnScreen extends BlendableBehavior {

	private float border;
	private float formula;
	
	public StayOnScreen(float border, float strength) {
		this.border = border;
		setStrength(strength);
	}
	
	
	@Override
	public void setStrength(float strength) {
		super.setStrength(strength);
		formula = strength / border;
	}
	
	
	public void apply(Entity boid, PApplet g) {
		PVector a = new PVector(0, 0);
		PVector p = boid.position();
		
		if (p.x < border)
			a.add(new PVector(formula * (border - p.x), 0));
		else if (p.x > g.width - border)
			a.add(new PVector(-formula * (border - (g.width - p.x)), 0));
		
		if (p.y < border)
			a.add(new PVector(0, formula * (border - p.y)));
		else if (p.y > g.height - border)
			a.add(new PVector(0, -formula * (border - (g.height - p.y))));
		
		if (draw()) {
			g.strokeWeight(2);
			g.stroke(0xff007700);
			g.line(p.x, p.y, p.x + AF * a.x, p.y + AF * a.y);
		}
		
		boid.accelerate(a);
	}
	
}

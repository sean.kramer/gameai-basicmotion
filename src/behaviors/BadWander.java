package behaviors;

import java.util.Random;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;


public class BadWander extends BlendableBehavior {

	private float seekDist = 200;
	private float seekRadius = 70;
	private float maxJumpRadians = PApplet.radians(80);
	private int smoothing = 3;
	
	public final Random RANDOM = new Random();

	private int buffer = 0;
	private float angle = 0;
	
	private SeekTarget seeker;
	
	/**
	 * Wander around aimlessly
	 * @param seekDist distance to center of seek-circle
	 * @param seekRadius radius of seek-circle
	 * @param maxJumpDegrees max degree change per update
	 * @param smoothing frames between updates
	 * @param strength power applied to the algorithm
	 */
	public BadWander(float seekDist, float seekRadius, float maxJumpDegrees, int smoothing, float strength) {
		super(strength);
		this.seekDist = seekDist;
		this.seekRadius = seekRadius;
		this.smoothing = smoothing;
		maxJumpRadians = PApplet.radians(maxJumpDegrees);
		
		seeker = new SeekTarget(0, 0, strength);
		addSubBehavior(seeker);
	}
	
	
	@Override
	public void apply(Entity boid, PApplet g) {
		if (buffer == 0) {
			seeker.target(RANDOM.nextInt(g.width), RANDOM.nextInt(g.height));
			buffer = smoothing;
		} else {
			buffer--;
		}
		
		applySubBehaviors(boid, g);
	}
}

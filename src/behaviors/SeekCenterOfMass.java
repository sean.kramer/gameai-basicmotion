package behaviors;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;

public class SeekCenterOfMass extends BlendableBehavior implements MultiEntityBehavior {

	private ArrayList<Entity> entities;
	private SeekTarget seeker;
	private float radius;
	
	public SeekCenterOfMass(float radius, float strength) {
		super(strength);
		this.radius = radius;
		this.seeker = new SeekTarget(0, 0, strength);
		entities = new ArrayList<Entity>();
		addSubBehavior(seeker);
	}

	
	@Override
	public void apply(Entity boid, PApplet g) {
		float x = 0, y = 0, m = 0;
		//Find an target center of mass
		for (Entity e : entities) {
			if (PVector.dist(e.position(), boid.position()) > radius)
				continue;
			x += e.position().x * e.getMass();
			y += e.position().y * e.getMass();
			m += e.getMass();
		}
		
		if (m == 0)
			return;
			
		if (draw()) {
			g.fill(0xffffff33);
			g.noStroke();
			g.ellipse(x / m, y / m, 10, 10);
		}
		
		seeker.target(x / m, y / m);
		applySubBehaviors(boid, g);
	}

	
	@Override
	public void addEntity(Entity entity) {
		entities.add(entity);
	}
}

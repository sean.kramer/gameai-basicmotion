package behaviors;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Crumb;
import entities.Entity;

public class BreadCrumbs extends Behavior {

	public static final int BUFFER_DISTANCE = 40;
	
	private float bufferDist;
	private float count;
	private int max;
	
	private PVector last = null;
	private ArrayList<Crumb> crumbs;

	public BreadCrumbs() {
		this(BUFFER_DISTANCE);
	}
	
	public BreadCrumbs(float bufferDist) {
		this(BUFFER_DISTANCE, 0);
	}
	
	
	public BreadCrumbs(float bufferDist, int max) {
		this.bufferDist = bufferDist;
		count = 0;
		crumbs = new ArrayList<Crumb>();
		this.max = max;
	}
	
	
	@Override
	public void apply(Entity boid, PApplet g) {
		if (last == null) {
			last = boid.position().get();
		} else {
			count += PVector.dist(boid.position(), last);
			if (count > bufferDist) {
				crumbs.add(new Crumb(boid.position().x, boid.position().y, boid.rotation()));
				count = 0;
			}
			last = boid.position().get();
		}
		
		if (max > 0 && crumbs.size() > max)
			crumbs.remove(0);
		
		for (Crumb c : crumbs)
			c.draw(boid, g);
	}

	
}

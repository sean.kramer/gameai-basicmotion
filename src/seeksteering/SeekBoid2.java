package seeksteering;
import basicmotion.BreadCrumb;
import processing.core.PApplet;
import processing.core.PVector;


public class SeekBoid2 {
	
	public static final float MAX_SPEED = 5;
	public static final float MAX_ACCELERATION = 0.8f;
	public static final float MAX_ROTATION = 0.15f;
	
	public static final float TARGET_SIZE = 6;
	
	public static final float RADIUS = 10;
	public static final float DIAMETER = RADIUS * 2;
	
	public static final float CRUMB_RAD_SQ = 30 * 30;
	
	public static final float RAD_OF_SAT_SQ = 20 * 20;
	public static final float MAX_ACCELERATION_SQ = MAX_ACCELERATION * MAX_ACCELERATION;
	
	public static final float TX = RADIUS * PApplet.cos(PApplet.radians(60));
	public static final float TY = RADIUS * PApplet.sin(PApplet.radians(60));
	public static final float TP = RADIUS * 2;
	
	public static final int COLOR = 0xff000000;
	
	
	private float rotation = 0;
	private PVector position;
	private PVector velocity;
	private PVector target;

	private BreadCrumb lastCrumb, crumb;
	
	public SeekBoid2(PVector position) {
		this.position = new PVector(position.x, position.y);
		this.target = new PVector(position.x, position.y);
		this.velocity = new PVector(0, 0);
		crumb = new BreadCrumb(position.x, position.y, rotation);
	}

	
	private void crumb() {
		if (crumb != null) return;
		float dx = position.x - lastCrumb.x;
		dx *= dx;
		float dy = position.y - lastCrumb.y;
		dy *= dy;
		if (dx + dy >= CRUMB_RAD_SQ)
			crumb = new BreadCrumb(position.x, position.y, rotation);
	}
	
	public void setTarget(PVector target) {
		this.target = target;
	}
	
	
	/** Apply current velocity to position. */
	public void step() {
		seek();
		position.x += velocity.x;
		position.y += velocity.y;
		crumb();
	}

	
	private void seek() {
		//Get a vector in direction of target
		PVector desired = new PVector(target.x - position.x, target.y - position.y);
		
	
		//Adjust acceleration with regards to velocity
		PVector delta;
		if (desired.magSq() > RAD_OF_SAT_SQ) {
			delta = PVector.sub(desired, velocity);
		//If within RADIUS OF SATISFACTION, try stop
		} else {
			delta = new PVector(-velocity.x, -velocity.y);
		}
		
		if (delta.magSq() > MAX_ACCELERATION_SQ) {
			delta.normalize();
			delta.mult(MAX_ACCELERATION);
		}
			
		/* Apply acceleration to velocity and then cap velocity at max speed. */
		velocity = PVector.add(velocity, delta);
		if (velocity.mag() > MAX_SPEED) {
			velocity.normalize();
			velocity.mult(MAX_SPEED);
		}

		if (velocity.magSq() > 0)
			matchRotation();
	}
	
	
	/** Steering behavior to align the orientation with the current velocity */
	private void matchRotation() {
		float desired = PApplet.atan2(velocity.y, velocity.x);
		
		float delta = desired - rotation;
		if (delta > PApplet.PI)
			delta -= PApplet.TWO_PI;
		if (delta <= -PApplet.PI)
			delta += PApplet.TWO_PI;
		
		if (Math.abs(delta) > MAX_ROTATION)
			delta = Math.signum(delta) * MAX_ROTATION;
		
		rotation += delta;
		if (rotation > PApplet.PI)
			rotation -= PApplet.TWO_PI;
		else if (rotation <= -PApplet.PI) {
			rotation += PApplet.TWO_PI;
		}
	}
	
	
	public PVector getPosition() {
		return position;
	}
	
	
	public PVector getVelocity() {
		return velocity;
	}
	
	public float getRotation() {
		return rotation;
	}
	
	
	public BreadCrumb getCrumb() {
		if (crumb == null)
			return null;
		lastCrumb = crumb;
		crumb = null;
		return lastCrumb;
	}
	
	
	public void draw(PApplet g) {
		g.fill(0xffff0000);
		g.ellipse(target.x, target.y, TARGET_SIZE, TARGET_SIZE);
		
		g.fill(COLOR);
		g.pushMatrix();
		g.translate(position.x, position.y);
		g.ellipse(0, 0, RADIUS * 2, RADIUS * 2);
		g.rotate(rotation);
		g.triangle(TX, -TY, TP, 0, TX, TY);
		g.popMatrix();
	}

}

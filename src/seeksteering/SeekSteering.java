package seeksteering;
import java.util.ArrayList;

import basicmotion.Boid;
import basicmotion.BreadCrumb;
import processing.core.PApplet;
import processing.core.PVector;


public class SeekSteering extends PApplet {

	static final long serialVersionUID = 1L;

	public static final float SCALE = 0.45f;
	
	private SeekBoid boid;
	private ArrayList<BreadCrumb> crumbs;
	
	public static void main(String[] args) {
		PApplet.main("seeksteering.SeekSteering");
	}

	
	public void setup() {
		if (frame == null) {
			System.err.println("Please run as java application, not applet.");
			System.exit(0);
		}
		
		crumbs = new ArrayList<BreadCrumb>();
		
		size((int) (displayWidth * SCALE), (int) (displayHeight * SCALE));
		frame.setResizable(true);
		
		boid = new SeekBoid(new PVector(Boid.RADIUS * 2, height - Boid.RADIUS - Boid.SPEED));
		//frameRate(3);
		ellipseMode(CENTER);
		noStroke();
		smooth();
	}
	
	
	public void mousePressed() {
		boid.setTarget(new PVector(mouseX, mouseY));
	}
	
	
	public void draw() {
		background(0xffcccccc);
		boid.step();
		
		BreadCrumb crumb = boid.getCrumb();
		if (crumb != null)
			crumbs.add(crumb);
		
		for (BreadCrumb c : crumbs)
			c.draw(this);
		boid.draw(this);
	}
}

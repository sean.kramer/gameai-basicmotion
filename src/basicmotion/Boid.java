package basicmotion;
import processing.core.PApplet;
import processing.core.PVector;


public class Boid {
	
	public static final float SPEED = 3;
	
	public static final float RADIUS = 10;
	public static final float DIAMETER = RADIUS * 2;
	
	public static final float CRUMB_RAD_SQ = 30 * 30;
	
	public static final float TX = RADIUS * PApplet.cos(PApplet.radians(60));
	public static final float TY = RADIUS * PApplet.sin(PApplet.radians(60));
	public static final float TP = RADIUS * 2;
	public static final int COLOR = 0xff000000;
	
	private float speed = 0;
	private float rotation = 0;
	private PVector position;
	private PVector direction;
	
	private BreadCrumb lastCrumb, crumb;

	
	public Boid(PVector position) {
		this.position = position;
		this.direction = new PVector(0, 0);
		crumb = new BreadCrumb(position.x, position.y, rotation);
		updateVelocity();
	}
	
	
	public void setMoving(boolean moving) {
		speed = moving ? SPEED : 0;
	}
	
	
	private void crumb() {
		if (crumb != null) return;
		float dx = position.x - lastCrumb.x;
		dx *= dx;
		float dy = position.y - lastCrumb.y;
		dy *= dy;
		if (dx + dy >= CRUMB_RAD_SQ)
			crumb = new BreadCrumb(position.x, position.y, rotation);
	}
	
	
	public void step() {
		position.x += direction.x * speed;
		position.y += direction.y * speed;
		crumb();
	}

	
	private void updateVelocity() {
		direction.x = PApplet.cos(rotation);
		direction.y = PApplet.sin(rotation);
	}
	
	
	public void adjustRotation(float rad) {
		rotation += rad;
		updateVelocity();
	}
	
	public void setRotation(float rad) {
		rotation = rad;
		updateVelocity();
	}
	
	
	public PVector getPosition() {
		return position;
	}
	
	
	public float getRotation() {
		return rotation;
	}
	
	
	public void draw(PApplet g) {
		g.fill(COLOR);
		g.pushMatrix();
		g.translate(position.x, position.y);
		g.ellipse(0, 0, RADIUS * 2, RADIUS * 2);
		g.rotate(rotation);
		g.triangle(TX, -TY, TP, 0, TX, TY);
		g.popMatrix();
	}


	public BreadCrumb getCrumb() {
		if (crumb == null)
			return null;
		lastCrumb = crumb;
		crumb = null;
		return lastCrumb;
	}


	public boolean offScreen(PApplet g) {
		float tmpx = position.x + direction.x * speed;
		float tmpy = position.y + direction.y * speed;
		
		if (tmpx <= RADIUS || tmpy <= RADIUS || tmpx >= g.width - RADIUS || tmpy >= g.height - RADIUS)
			return true;
		
		return false;
	}
	
}

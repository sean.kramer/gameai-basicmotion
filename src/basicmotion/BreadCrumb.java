package basicmotion;
import processing.core.PApplet;


public class BreadCrumb {

	public static final float RADIUS = 4;
	public static final float DIAMETER = RADIUS * 2;
	
	public static final float TX = RADIUS * PApplet.cos(PApplet.radians(60));
	public static final float TY = RADIUS * PApplet.sin(PApplet.radians(60));
	public static final float TP = RADIUS * 2;
	
	public static final int COLOR = 0xffbb8855;
	
	public float x, y, rotation;
	
	public BreadCrumb(float x, float y, float rotation) {
		this.rotation = rotation;
		this.x = x;
		this.y = y;
	}


	public void draw(PApplet g) {
		g.fill(COLOR);
		g.pushMatrix();
		g.translate(x, y);
		g.ellipse(0, 0, DIAMETER, DIAMETER);
		g.rotate(rotation);
		g.triangle(TX, -TY, TP, 0, TX, TY);
		g.popMatrix();
	}
	
}

package basicmotion;
import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;
import processing.event.KeyEvent;


public class BasicMotion extends PApplet {

	static final long serialVersionUID = 1L;

	public static final float SCALE = 0.45f;
	
	private Boid boid;
	private ArrayList<BreadCrumb> crumbs;
	
	public static void main(String[] args) {
		PApplet.main("basicmotion.BasicMotion");
	}
	
	
	private float rotation = 0;
	public void keyPressed(KeyEvent e) {
		switch (e.getKey()) {
		case 'w': boid.setMoving(true); break;
		case 'd': rotation = .1f; break;
		case 'a': rotation = -.1f;
		}
	}
	
	
	public void keyReleased(KeyEvent e) {
		switch (e.getKey()) {
		case 'w': boid.setMoving(false); break;
		case 'd': rotation = 0; break;
		case 'a': rotation = 0; break;
		case 'r': crumbs.clear(); break;
		case ' ': boid.setMoving(true); break;
		}
	}
	
	
	public void setup() {
		size((int) (displayWidth * SCALE), (int) (displayHeight * SCALE));
		frame.setResizable(true);
		
		crumbs = new ArrayList<BreadCrumb>();
		boid = new Boid(new PVector(Boid.RADIUS * 2, height - Boid.RADIUS - Boid.SPEED));
		boid.setMoving(true);
		
		ellipseMode(CENTER);
		noStroke();
		smooth();
	}
	
	
	private int rotateCounter = 0;
	public void draw() {
		background(0xffcccccc);
		
		boid.adjustRotation(rotation);
		boid.step();
		
		
		if (boid.offScreen(this)) {
			boid.adjustRotation(-PI / 2);
			rotateCounter++;
			if (rotateCounter == 4) {
				boid.setMoving(false);
				rotateCounter = 0;
			}
		}
		
		BreadCrumb crumb = boid.getCrumb();
		if (crumb != null)
			crumbs.add(crumb);
		
		for (BreadCrumb c : crumbs)
			c.draw(this);
		boid.draw(this);
	}
}
